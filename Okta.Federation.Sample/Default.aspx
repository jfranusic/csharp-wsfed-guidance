﻿<%@ Page Title="Okta SSO Sample Web Application" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" EnableViewState="false" Inherits="Okta.Federation.Sample._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <div class="jumbotron">
        <asp:LoginView ID="loginView" runat="server">
            <AnonymousTemplate>
                <h2>Hello anonymous</h2>
                <p>You haven't completed SSO yet, please login.</p>
                <p>
                    <a class="btn btn-lg btn-primary" href="Login.aspx"><span class="glyphicon glyphicon-user"> Sign In</span></a>
                </p>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <h2>
                    Hello <asp:LoginName ID="jumbotronLoginName" runat="server" />
                </h2>
                <p>Congratulations, you have successfully SSO authenticated with Okta!</p>
                <h2><span class="glyphicon glyphicon-user"></h2>
            </LoggedInTemplate>
        </asp:LoginView>

    </div>
    <div class="container">
        <h1><%: Page.Title %></h1>
        <h2>Demonstrates SSO with Okta</h2>
        <p>This is a sample ASP.NET Web Application that demonstrates Single Sign-On integration with Okta using Windows Identity Foundation (WIF) and claims-based authentication.</p>
        <p>
            <img src="Images/okta-aspnet.png" />
        </p>
        <p>
            During SSO authentication Okta will pass identity attributes about your user (claims).  These issued claims are available on your current user's principal as an IClaimsPrincipal.
        </p>
        <pre>Page.User as IClaimsPrincipal</pre>
        <p>
            For more information on claims programming, please refer to <a href="http://msdn.microsoft.com/en-us/library/ff423674.aspx">A Guide to Claims-Based Identity and Access Control</a>.
            <br />
            <em>Note: Okta replaces the role of ADFS in all MSDN documentation.</em>
        </p>
    </div>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2 id="identity">Identity</h2>
    <div class="panel panel-primary">
        <div class="panel-heading">Page.User.Identity</div>
        <div class="panel-body">
            <asp:GridView ID="identityGridView" runat="server" CssClass="table table-striped table-condensed" GridLines="None" BorderWidth="0" ShowHeader="False">
            </asp:GridView>
        </div>
    </div>
    <h2 id="claims">Claims</h2>
    <div class="panel panel-primary">
        <div class="panel-heading">Your claims</div>
        <div class="panel-body">
            <asp:GridView ID="claimsGridView" runat="server" CssClass="table table-striped table-condensed" GridLines="None" BorderWidth="0">
            </asp:GridView>
        </div>
    </div>
</asp:Content>
