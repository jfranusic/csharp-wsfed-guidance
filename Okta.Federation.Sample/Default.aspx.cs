﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.UI;


namespace Okta.Federation.Sample
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var claimsPrincipal = Page.User as ClaimsPrincipal;

            this.identityGridView.DataSource = GetIdentitySource();
            this.identityGridView.DataBind();

            this.claimsGridView.DataSource = claimsPrincipal.Claims.Select(c => {
                return new { ClaimType = c.Type, Issuer = c.Issuer, ClaimValue = c.Value }; 
            });
            this.claimsGridView.DataBind();
        }

        IEnumerable<Tuple<string, string>> GetIdentitySource()
        {
            yield return new Tuple<string, string>("Name", Page.User.Identity.Name);
            yield return new Tuple<string, string>("Is Authenticated", Page.User.Identity.IsAuthenticated.ToString());
            yield return new Tuple<string, string>("Authentication Type", Page.User.Identity.AuthenticationType);
        }
    }
}