﻿using System;
using System.IdentityModel.Services;
using System.Web.UI;

namespace Okta.Federation.Sample
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var authModule = Context.ApplicationInstance.Modules["WSFederationAuthenticationModule"] as WSFederationAuthenticationModule;
            var ssoRequest = authModule.CreateSignInRequest(Guid.NewGuid().ToString(), "/Default.aspx" , false);
            Page.Response.Redirect(ssoRequest.RequestUrl);

        }
    }
}