﻿using System;
using System.IdentityModel.Services;
using System.Web.UI;

namespace Okta.Federation.Sample
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void signInButton_Click(object sender, EventArgs e)
        {
            var authModule = Context.ApplicationInstance.Modules["WSFederationAuthenticationModule"] as WSFederationAuthenticationModule;
            var ssoRequest = authModule.CreateSignInRequest(Guid.NewGuid().ToString(), Request.RawUrl, false);
            Page.Response.Redirect(ssoRequest.RequestUrl);
        }


    }
}