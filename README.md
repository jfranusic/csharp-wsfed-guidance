# Introduction

This project contains sample code that shows how configure an ASP.NET application to support Single Sign On via WS Federation.

# Setup and Configuration

1.  The code in this project assumes that you have an Okta organization, get your own Okta organization by signing up for [Okta Developer Edition](http://developer.okta.com/).
2.  Once you have your Okta organization, follow the guide for [configuring an Okta Template WS Federation Application](https://support.okta.com/articles/Knowledge_Article/29510977-Configuring-the-Okta-Template-WS-Federation-Application)
3. At the end of the guide above, you will see a "ASP.NET 4.5" configuration in the "Sample ASP.NET Configuration". Copy this configuration into your clipboard.
4.  Open the "Web.config" file in this project and paste in the sample configuration from your clipboard.

# Running

With this sample project open Visual Studio:

*  Follow the steps in the "Setup and Configuration" section above.
*  From the "Debug" menu, select "Start Debugging". Or press the "F5" button.
*  Internet Explorer will open. You will be prompted to enter your credentials into Okta, then you will see the Default.aspx page with your user information in it.